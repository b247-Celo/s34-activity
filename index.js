/*const express = require("express");


	const app = express();

	const port = 4004;

	app.use(express.json());

	app.use(express.urlencoded({extended: true}));

	app.listen(port, () => console.log(`Server is running at localhost: ${port}`) );

	//[ SECTION ] Routes

	app.get("/hello", (request, response) => {
		response.send('Hello from the /hello endpoint');
	} );

	app.post("/display-name", (request, response) => {
		response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
	} );

	let users = [];

	app.post("/sign-up", (req, res) => {
		if(req.body.username !== "" && req.body.password !== "") {
			users.push(req.body);
			res.send(`User ${req.body.username} succesfully registered`);

		} else {
			res.send("Please input BOTH username and password!");
		}
	})

	app.put("/change-password", (req, res) => {
		let message;

		for(let i = 0; i < users.length; i++) {
		if(req.body.username ==  users[i].username) {
			users[i].password = req.body.password;

			message -= `User ${req.body.username}'s password has been updated.`;

			break;

			 } else {
			 	message = "User does not exist!"
			 }
		}
		
		res.send(message);

		})	

	*/ 

	//[ACTIVITY S34]

	const express = require("express");


	const app = express();

	const port = 3000;

	app.use(express.json());

	app.use(express.urlencoded({extended: true}));

	app.listen(port, () => console.log(`Server is running at localhost: ${port}`) );

	app.get("/home", (request, response) => {
		response.send('Welcome to the home page');
	} );

	let users = [
			{

			"username": "JohnDoe",
			"password": "johndoe123"
			}
			]

		app.get("/users", (request, response) => {
			response.send(users);
		} );

	let user = [];

	app.delete("/delete-user", (req, res) => {
		if(req.body.username !== "")
		 {
			user.push(req.body);
			res.send(`User ${req.body.username} has been deleted`);
		}
		} );